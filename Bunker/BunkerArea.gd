extends Area2D

@export var sprite: Sprite2D
@export var textures: Array[Texture2D]

var damage = 0
const TOTAL_DAMAGE = 3


func _ready():
	self.area_entered.connect(_self_area_entered)

func _self_area_entered(area):
	if area is Bullet || area is Rocket:
		area.queue_free()
		if damage < TOTAL_DAMAGE:
			sprite.texture = textures[damage]
			damage += 1
		else:
			queue_free()
	elif area is Invader:
		queue_free()
