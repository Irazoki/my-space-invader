extends CanvasLayer

var life_texture = preload("res://Player/Player.png")
@onready var lifes_ui_container = $MarginContainer/HBoxContainer
@onready var points = $MarginContainer/Points
var points_value := 0
var lifes := 3

func _ready():
	points.text = "SCORE: %d" % points_value 	
	Events.add_points.connect(_add_points)   	
	Events.you_die.connect(_remove_life)  	 
	_draw_lifes()
	
func _draw_lifes() -> void:
	for life in lifes_ui_container.get_children():
		life.queue_free()
		
	for i in range(lifes):
		var life_texture_rect = TextureRect.new()
		life_texture_rect.expand_mode = TextureRect.EXPAND_KEEP_SIZE
		life_texture_rect.custom_minimum_size = Vector2(40, 25)
		life_texture_rect.texture_filter = CanvasItem.TEXTURE_FILTER_NEAREST
		life_texture_rect.texture = life_texture
		lifes_ui_container.add_child(life_texture_rect)

func _add_points(add_points: int) -> void:
	points_value += add_points
	points.text = "SCORE: %d" % points_value
	
func _remove_life() -> void:
	lifes -= 1
	if lifes >= 0:
		_draw_lifes()
	else:
		Events.emit_signal("you_lose")
