extends Area2D

@onready var _animated_sprite := $AnimatedSprite2D
@onready var _sprite := $Sprite
@onready var _marker := $Marker2D
@onready var _collision_shape = $CollisionShape2D

@export var speed := 200
var direction := Vector2.ZERO
var bullet = preload("res://Player/Bullet.tscn") 
var player_width := 0.0
var can_shoot := true


func _ready():
	_start_position()	

func _process(delta):
	var input := Input.get_axis("left", "right")
	if input > 0:		
		direction = Vector2.RIGHT
	elif input < 0:
		direction = Vector2.LEFT
	else:
		direction = Vector2.ZERO
	
	var new_position = position.x + (direction.x * speed * delta)
	
	if new_position < player_width or new_position > get_viewport().get_visible_rect().end.x - player_width:
		return
	position.x = new_position

func _input(event):
	if event.is_action_pressed("shoot") && can_shoot:
		shoot()

func shoot() -> void:
	can_shoot = false
	var new_bullet = bullet.instantiate()
	new_bullet.global_position = _marker.global_position
	get_tree().root.add_child(new_bullet)
	new_bullet.tree_exited.connect(on_bullet_destroyed)

func on_bullet_destroyed():
	can_shoot = true
	
func _on_area_entered(_area):
	call_deferred("_sprite_toggle")
	Events.emit_signal("you_die")	
	_animated_sprite.play("die")


func _on_animated_sprite_2d_animation_finished():	
	call_deferred("_sprite_toggle")
	_start_position()

func _sprite_toggle() -> void:
	set_process(!is_processing())
	set_process_input(!is_processing_input())
	_collision_shape.disabled = !_collision_shape.disabled
	_sprite.visible = !_sprite.visible
	_animated_sprite.visible = !_animated_sprite.visible

func _start_position() -> void:
	var rect = get_viewport().get_visible_rect()
	player_width = transform.get_scale().x * (_sprite.texture.get_width() /2)
	global_position.x = rect.size.x / 2
	global_position.y = rect.size.y - (transform.get_scale().y * (_sprite.texture.get_height() /2))
