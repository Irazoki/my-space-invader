extends Node2D

@onready var _invaders = $Invaders

const ROWS = 5
const COLS = 11
const HORIZONTAL_SPACING = 32
const VERTICAL_SPACING = 16
const HORIZONTAL_MOVE = Vector2(10, 0)
const VERTICAL_MOVE = Vector2(0, 10)

@export var start_y : float = 80.0
var start_x : float = 0.0
var direction : Vector2 = Vector2.LEFT
var invader_width : int = 0
var invader_height : int = 0
var invader_scale_x : float = 0.0
var invader_scale_y : float = 0.0
var viewport : Vector2 = Vector2.ZERO

var invader1 = preload("res://Invaders/Invader1.tscn")
var invader2 = preload("res://Invaders/Invader2.tscn")
var invader3 = preload("res://Invaders/Invader3.tscn")
var rocket := preload("res://Invaders/Rocket.tscn")

func _ready():
	Events.wall_area_entered.connect(_on_wall_area_entered)
	Events.enemy_killed.connect(_count_invaders)
	var invader
	viewport = get_viewport().get_visible_rect().size	
	for row in ROWS:
		if row == 0:			
			invader = invader1
			calculate_start_position(invader)
		elif row == 1 or row == 2:
			invader = invader2
			calculate_start_position(invader)
		else:
			invader = invader3
			calculate_start_position(invader)
		
		spawn_invaders(row, invader)

func calculate_start_position(invader: PackedScene) -> void:
	var new_invader = invader.instantiate()
	var animated_sprite = new_invader.get_node("AnimatedSprite2D")
	var texture = animated_sprite.sprite_frames.get_frame_texture("move", 0)		
	
	invader_width = texture.get_width()
	invader_height = texture.get_height()
	invader_scale_x = animated_sprite.scale.x
	invader_scale_y = animated_sprite.scale.y
	
	start_x = (viewport.x - ((HORIZONTAL_SPACING * (COLS - 1)) + (invader_width * invader_scale_x * COLS))) / 2	
	
	new_invader.queue_free()
	
func spawn_invaders(row: int, invader: PackedScene) -> void:	
	for col in COLS:
		var new_invader = invader.instantiate()		
		var x = start_x + (col * HORIZONTAL_SPACING) + (col * invader_scale_x * invader_width)
		var y = start_y + (row * VERTICAL_SPACING) + (row * invader_scale_y * invader_height)
		new_invader.global_position = Vector2(x, y)
		_invaders.add_child(new_invader)
		

func _on_move_timer_timeout():
	var new_position = _invaders.position + (HORIZONTAL_MOVE * direction)	
	_invaders.position = new_position
	
func _on_wall_area_entered(new_direction: Vector2):	
	direction = new_direction
	_invaders.position += VERTICAL_MOVE


func _on_shot_timer_timeout():
	if _invaders.get_children().size() == 0:
		return
	var random_child = _invaders.get_children().pick_random()	
	var new_rocket = rocket.instantiate()
	new_rocket.global_position = random_child.global_position
	get_tree().current_scene.add_child(new_rocket)

func _count_invaders() -> void:
	if _invaders.get_children().size() == 1:		
		Events.emit_signal("you_win")
