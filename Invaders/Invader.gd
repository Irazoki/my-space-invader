extends Area2D
class_name Invader

@onready var _animated_sprite := $AnimatedSprite2D

var points := 10 : set = set_points, get = get_points

func set_points(new_points: int) -> void:
	points = new_points

func get_points() -> int:
	return points

func _on_area_entered(_area):
	Events.emit_signal("enemy_killed")	
	Events.emit_signal("add_points", points)
	_animated_sprite.play("explode")


func _on_animated_sprite_2d_animation_finished():
	if _animated_sprite.animation == "explode":
		queue_free()
		
		
