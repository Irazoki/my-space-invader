extends CanvasLayer
@onready var texture1 := $MarginContainer/VBoxContainer/InvaderContainer1/TextureRect1
@onready var label1 := $MarginContainer/VBoxContainer/InvaderContainer1/Label1
@onready var texture2 := $MarginContainer/VBoxContainer/InvaderContainer2/TextureRect2
@onready var label2 := $MarginContainer/VBoxContainer/InvaderContainer2/Label2
@onready var texture3 := $MarginContainer/VBoxContainer/InvaderContainer3/TextureRect3
@onready var label3 := $MarginContainer/VBoxContainer/InvaderContainer3/Label3
@onready var timer := $Timer

var elements = []

func _ready():
	elements = [texture1, label1, texture2, label2, texture3, label3]

func _on_button_pressed():
	get_tree().change_scene_to_file("res://Scenes/Game.tscn")


func _on_timer_timeout():	
	var element = elements.pop_front()
	if element:
		element.visible = true
	else:
		timer.stop()
		timer.queue_free()
