extends Node2D

@onready var _lose_screen := $LoseScreen
@onready var _win_screen := $WinScreen

func _ready():	
	Events.you_lose.connect(_end_screen)
	Events.you_win.connect(_winner_screen)		

func _end_screen() -> void:
	get_tree().paused = true
	_lose_screen.visible = true	
	
func _winner_screen() -> void:	
	get_tree().paused = true
	_win_screen.visible = true
	
	
	
