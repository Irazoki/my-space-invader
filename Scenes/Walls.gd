extends Node

func _on_left_wall_area_entered(_area):
	Events.emit_signal("wall_area_entered", Vector2.RIGHT)

func _on_right_wall_area_entered(_area):
	Events.emit_signal("wall_area_entered", Vector2.LEFT)

func _on_floor_area_entered(_area):
	Events.emit_signal("you_lose")
