extends CanvasLayer

var is_paused : bool = false : set = set_is_paused, get = get_is_paused


func _ready():
	Events.you_lose.connect(_disable_input)
	Events.you_win.connect(_disable_input)

func _disable_input() -> void:
	set_process_unhandled_input(false)

func set_is_paused(value: bool) -> void:
	is_paused = value
	get_tree().paused = is_paused
	visible = is_paused
	
func get_is_paused() -> bool:
	return is_paused
	
func _unhandled_input(event):
	if event.is_action_pressed("pause"):
		is_paused = !is_paused

func _on_restart_button_pressed():
	is_paused = false
	get_tree().reload_current_scene()


func _on_resume_button_pressed():
	is_paused = false


func _on_start_menu_button_pressed():
	is_paused = false
	get_tree().change_scene_to_file("res://Scenes/StartMenu.tscn")
