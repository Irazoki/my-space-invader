extends Node

signal add_points(points: int)
signal you_lose()
signal you_die()
signal you_win()
signal enemy_killed()
signal wall_area_entered(new_direction: Vector2)
