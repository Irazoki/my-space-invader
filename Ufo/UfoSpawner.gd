extends Node2D

@export var min_timer = 5
@export var max_timer = 10

@onready var _ufo_timer := $UfoTimer

var ufo = preload("res://Ufo/Ufo.tscn")

func _ready():
	_ufo_timer.wait_time = randi_range(min_timer, max_timer) 
	_ufo_timer.start()

func _on_ufo_timer_timeout():
	var new_ufo = ufo.instantiate()
	add_child(new_ufo)	
	_ufo_timer.wait_time = randi_range(min_timer, max_timer) 
	
	
