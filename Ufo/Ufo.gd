extends Area2D

@export var speed := 150.0
@export var points := 100
@export var die_texture : Texture2D

@onready var _sprite = $Sprite2D
@onready var _marker = $Marker2D
@onready var _shot_timer = $ShotTimer
@onready var _die_timer = $DieTimer

var rocket = preload("res://Invaders/Rocket.tscn")

func _ready():
	position.x = get_viewport().get_visible_rect().end.x + (_sprite.transform.get_scale().x * (_sprite.texture.get_width() /2))
	position.y = _sprite.transform.get_scale().y * (_sprite.texture.get_height() /2) + 60
	
func _process(delta):
	position.x -= delta * speed

func _on_timer_timeout():	
	var new_rocket = rocket.instantiate()
	new_rocket.global_position = _marker.global_position
	new_rocket.modulate = Color(0.67, 0.2, 0.2, 1)
	get_tree().current_scene.add_child(new_rocket)

func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()

func _on_area_entered(area):
	if area is Bullet:
		_shot_timer.stop()
		set_process(false)
		Events.emit_signal("add_points", points)
		_sprite.texture = die_texture
		_die_timer.start()

func _on_die_timer_timeout():
	queue_free()
